The terms:

'rocksolid light', 'rslight', 'retro guy', 'novabbs' are the terms of art that
belong to Retro Guy. The enclosed license does not grant permission to label,
market, advertise, use, buy, sell or trade in these terms. Contact Retro Guy at
https://novabbs.org regarding permission to use said terms.

