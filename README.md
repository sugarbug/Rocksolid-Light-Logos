RockSolidLight Logo Samples
===========================

Use of this software is subject to the copyright notice and trademark rights of
terms and license included herewith.

by SugarBug | http://syfershock.com/users/syfershock

This directory contains samples of logos for the RockSolidLight (rslight)
NNTP forums [https://novabbs.com]. Several different colors and patterns are
provided in the hope they might match custom web page themes and layouts.

/PNG/ contains images in portable network graphics format.

/SVG/ contains images in scalable vector graphics format.

/latest_buttons_svg/ are scalable SVG images in ten colors and five designs.

The PNG images have smaller file sizes but do not scale well.

The SVG images scale crisply to any size set in CSS styles. It is almost always
better to use SVG images since they will look sharp on every display.

Folder structure
----------------

+ ./latest_buttons_svg
+ ./latest_buttons_svg/chevron.down
+ ./latest_buttons_svg/hashtag.circle
+ ./latest_buttons_svg/chevron.right
+ ./latest_buttons_svg/diamond
+ ./latest_buttons_svg/triangle
+ ./latest_buttons_svg/checker
+ ./PNG
+ ./PNG/text-art
+ ./PNG/text-art/blue-purple
+ ./PNG/text-art/rainbow
+ ./PNG/text-art/white-flare
+ ./PNG/logos
+ ./SVG
